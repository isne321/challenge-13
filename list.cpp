#include <iostream>
#include "list.h"
using namespace std;

void List::headPush(int var) {

	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		head = new Node(var, 0, 0);
		tail = head;
	}
	else {
		tmp = new Node(var, head, 0);
		head->prev = tmp;
		head = tmp;
	}

}

void List::tailPush(int var) {

	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		tail = new Node(var, 0, tail); // Don't need tmp ;)
		head = tail;
	}
	else {
		tmp = new Node(var, 0, tail); // Don't need tmp ;)
		tail->next = tmp;
		tail = tmp;
	}

}

int List::headPop() {

	if (isEmpty()) {
		cout << "Empty list.";
		return false;
	}
	Node *tmp = head->next;
	delete head;
	head = tmp;
	head->prev = 0;

	return head->info;

}

int List::tailPop() {

	if (isEmpty()) {
		cout << "Empty list.";
		return false;
	}

	Node *tmp;
	tmp = tail->prev;
	delete tail;
	tail = tmp;
	tmp->next = 0;
	return tail->info;

}

bool List::isInList(int search) {

	if (isEmpty()) {
		cout << "Empty list.";
		return false;
	}

	Node *tmp = head;
	bool result;
	result = false;
	cout << "Serach : " << search << " IN( ";
	while (true) {
		cout << " " << tmp->info << " ";
		if (tmp->info == search) {
			result = true;
		}
		tmp = tmp->next; // next pointer
		if (tmp == 0)break;

	}
	cout << " )";
	return result; // return true if found

}

void List::deleteNode(int search) {

	if (isEmpty()) {
		cout << "Empty list.";
		return;
	}

	Node *tmp = head; // pointer
	while (true) {
		if (tmp->info == search) {
			if (tmp == tail) { // check last element
				List::tailPop(); //tailpop ;)
				return;
			}
			else if (tmp == head) {
				List::headPop(); //headpop ;)
				return;
			}
			else {
				Node *copy = tmp->prev;
				copy->next = tmp->next;
				tmp->next->prev = copy;
				delete tmp;  // delete tmp
				tmp = copy;  // tmp_2 -> skip
				return;

			}
		}

		tmp = tmp->next;

		if (tmp == 0) return;

	}
}

void List::Sort() {

	Node *ptr1 = head; // pointer
	int tmp;
	bool swap = false;

	while (true) {
		swap = false;
		while (true) {
			if (ptr1->info > ptr1->next->info) {
				tmp = ptr1->next->info;
				ptr1->next->info = ptr1->info; //swap value only
				ptr1->info = tmp; //swap value only
				swap = true;
			}
			ptr1 = ptr1->next;
			if (ptr1 == tail) {
				break;
			}
		}

		if (swap == false) {
			return;
		}
		ptr1 = head;

	}

}

void List::Unique() {

	Node *ptr1 = head; // pointer
	Node *ptr2 = head;// pointer
	int del;

	while (true) {
		while (true) {
			if (ptr1 != ptr2) {
				if (ptr1->info == ptr2->info) {
					del = ptr1->info;
					ptr1 = ptr1->next;
					cout << "delete duplicate: " << del << endl;
					List::deleteNode(del);
				}
			}
			if (ptr2->next == 0) {
				break;
			}
			ptr2 = ptr2->next;
		}
		ptr1 = ptr1->next;
		if (ptr1 == 0) {
			return;
		}
		ptr2 = head;
	}

}

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

